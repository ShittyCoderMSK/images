Вот пример bash-скрипта, который обнаруживает видеокарту NVIDIA в системе, устанавливает соответствующие драйверы и активирует их в дисплейном менеджере LightDM. Скрипт предназначен для Debian/Ubuntu.

### Скрипт

```bash
#!/bin/bash

# Проверка наличия видеокарты NVIDIA
if lspci | grep -i nvidia; then
    echo "NVIDIA видеокарта обнаружена. Установка драйверов..."

    # Обновление списка пакетов
    sudo apt update

    # Установка пакетов для добавления PPA
    sudo apt install -y software-properties-common

    # Добавление PPA для драйверов NVIDIA
    sudo add-apt-repository ppa:graphics-drivers/ppa -y

    # Обновление списка пакетов после добавления PPA
    sudo apt update

    # Установка рекомендуемого драйвера NVIDIA
    sudo apt install -y nvidia-driver-470  # Замените номер версии на актуальный для вашей системы

    # Установка утилит для настройки NVIDIA
    sudo apt install -y nvidia-settings nvidia-prime

    # Автоматическая настройка X-сервера для использования драйвера NVIDIA
    sudo nvidia-xconfig

    # Настройка LightDM для использования драйвера NVIDIA
    if [ -f /etc/lightdm/lightdm.conf ]; then
        sudo sed -i '/\[Seat:\*\]/a display-setup-script=/usr/bin/nvidia-settings --load-config-only' /etc/lightdm/lightdm.conf
    else
        echo "[Seat:*]" | sudo tee -a /etc/lightdm/lightdm.conf
        echo "display-setup-script=/usr/bin/nvidia-settings --load-config-only" | sudo tee -a /etc/lightdm/lightdm.conf
    fi

    # Перезагрузка для применения изменений
    echo "Установка завершена. Перезагрузка системы для применения изменений..."
    sudo reboot
else
    echo "NVIDIA видеокарта не обнаружена. Установка драйверов не требуется."
fi
```

### Объяснение

1. **Проверка наличия видеокарты NVIDIA**:
   ```bash
   if lspci | grep -i nvidia; then
   ```
   Эта строка использует `lspci` для получения списка всех PCI устройств и `grep` для поиска строки, содержащей "nvidia". Если строка найдена, условие выполняется.

2. **Обновление списка пакетов и установка необходимых пакетов**:
   ```bash
   sudo apt update
   sudo apt install -y software-properties-common
   ```
   Эти команды обновляют список пакетов и устанавливают `software-properties-common`, который необходим для добавления PPA.

3. **Добавление PPA для драйверов NVIDIA**:
   ```bash
   sudo add-apt-repository ppa:graphics-drivers/ppa -y
   sudo apt update
   ```
   Эти команды добавляют репозиторий PPA для драйверов NVIDIA и обновляют список пакетов.

4. **Установка рекомендуемого драйвера NVIDIA**:
   ```bash
   sudo apt install -y nvidia-driver-470
   ```
   Эта команда устанавливает драйвер NVIDIA. Вы можете изменить номер версии на актуальный для вашей системы.

5. **Установка утилит для настройки NVIDIA**:
   ```bash
   sudo apt install -y nvidia-settings nvidia-prime
   ```
   Эти команды устанавливают утилиты для управления настройками NVIDIA и переключения графических режимов.

6. **Автоматическая настройка X-сервера**:
   ```bash
   sudo nvidia-xconfig
   ```
   Эта команда автоматически создает конфигурационный файл для X-сервера, чтобы использовать драйвер NVIDIA.

7. **Настройка LightDM для использования драйвера NVIDIA**:
   ```bash
   if [ -f /etc/lightdm/lightdm.conf ]; then
       sudo sed -i '/\[Seat:\*\]/a display-setup-script=/usr/bin/nvidia-settings --load-config-only' /etc/lightdm/lightdm.conf
   else
       echo "[Seat:*]" | sudo tee -a /etc/lightdm/lightdm.conf
       echo "display-setup-script=/usr/bin/nvidia-settings --load-config-only" | sudo tee -a /etc/lightdm/lightdm.conf
   fi
   ```
   Эта часть скрипта проверяет наличие файла конфигурации LightDM и добавляет строку для запуска `nvidia-settings` при запуске LightDM.

8. **Перезагрузка системы**:
   ```bash
   sudo reboot
   ```
   Для применения изменений требуется перезагрузка системы.

### Использование

Сохраните этот скрипт в файл, например, `install_nvidia_driver.sh`, и сделайте его исполняемым:

```bash
chmod +x install_nvidia_driver.sh
```

Запустите скрипт:

```bash
sudo ./install_nvidia_driver.sh
```

Этот скрипт автоматически проверит наличие видеокарты NVIDIA, установит соответствующие драйверы, настроит X-сервер и LightDM для использования драйвера NVIDIA, а затем перезагрузит систему для применения изменений.